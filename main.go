package main

import (
	"bytes"
	"crypto/rand"
	"crypto/rsa"
	"crypto/x509"
	"encoding/pem"
	"errors"
	"flag"
	"fmt"
	"io/ioutil"
	"log"
	mathRand "math/rand"
	"os"
	"strings"
	"text/template"
	"time"

	"github.com/SophisticaSean/easyssh"
	petname "github.com/dustinkirkland/golang-petname"
	"github.com/packethost/packngo"
	"golang.org/x/crypto/ssh"
)

var (
	githubDeployKey = flag.String("github-deploy-key", "$HOME/.ssh/id_rsa", "path to github deploy key")
	createServer    = flag.Bool("create-server", true, "whether to launch a new build-doer server")
	existingDevice  = flag.String("existing-device", "", "existing device name; if provided, prevents launch")
)

func main() {
	flag.Parse()

	tkn := os.Getenv("PACKET_TOKEN")

	c := packngo.NewClient("test", tkn, nil)

	projectID, err := GetProjectIDByName(c, "buildy")
	if err != nil {
		log.Fatal(err)
	}

	var d *packngo.Device

	if *existingDevice == "" {
		err := MakeSSHKeyPair("pub", "priv")
		if err != nil {
			log.Fatal(err)
		}
		cloudInitConfig, err := NewCloudInitConfig("pub")
		if err != nil {
			log.Fatal(err)
		}
		dcr := packngo.DeviceCreateRequest{}
		dcr.HostName = NewServerName()
		dcr.Tags = []string{"build-doer"}
		dcr.ProjectID = projectID
		//dcr.Plan = "baremetal_0"
		dcr.Plan = "baremetal_1"
		dcr.Facility = "ewr1"
		dcr.OS = "ubuntu_18_04"
		//dcr.OS = "centos_7"
		dcr.BillingCycle = "hourly"
		dcr.UserData = cloudInitConfig
		d, err = LaunchNewServer(c, &dcr, cloudInitConfig)
		if err != nil {
			log.Fatal(err)
		}
	} else {
		d, err = GetDeviceIDByName(c, projectID, *existingDevice)
		if err != nil {
			log.Fatal(err)
		}

	}

	conf := easyssh.SSHConfig{}
	conf.User = "coleman"
	conf.Key = "priv"
	conf.Server = d.Network[0].Address
	scpFile(&conf, os.ExpandEnv(*githubDeployKey), "/home/coleman/.ssh/id_rsa")
	runCommand(&conf, "sudo apt-get update", 300)
	runCommand(&conf, "sudo apt-get install git curl gcc open-iscsi multipath-tools -y", 300)
	runCommand(&conf, "curl https://sh.rustup.rs -sSf | sh -s -- -y --default-toolchain \"nightly\"", 600)
	runCommand(&conf, "curl -fsSL https://deno.land/x/install/install.sh | sh", 300)
	runCommand(&conf, "ssh-keyscan github.com >> ~/.ssh/known_hosts", 20)
	runCommand(&conf, `echo 'export PATH="/home/coleman/.deno/bin:$PATH"' >> .profile`, 20)

	fmt.Printf("ssh -i priv coleman@%s\n", conf.Server)
}

func LaunchNewServer(c *packngo.Client, dcr *packngo.DeviceCreateRequest, cloudInitConfig string) (*packngo.Device, error) {
	// TODO factor this out into a function

	dev, _, err := c.Devices.Create(dcr)
	if err != nil {
		log.Fatal(err)
	}
	ticker := time.NewTicker(10 * time.Second)
	devID := dev.ID

	var provisioned bool

	for {
		select {
		case <-ticker.C:
			d, _, err := c.Devices.Get(devID)
			if err != nil {
				log.Fatal(err)
			}
			fmt.Printf("device state: %s\n", d.State)
			if strings.TrimSpace(d.State) == "active" {
				time.Sleep(2 * time.Second)
				provisioned = true
			}
		}
		if provisioned {
			break
		}
	}
	d, _, err := c.Devices.Get(devID)
	return d, err
}

func runCommand(conf *easyssh.SSHConfig, cmd string, timeoutSec int) {
	out, errStr, isTimeout, err := conf.Run(cmd, timeoutSec)
	if err != nil {
		// TODO return an error here? What to do if this fails?
		log.Fatal(err)
	}
	fmt.Println("outstr:", out)
	fmt.Println("errstr:", errStr)
	fmt.Println("timeout:", isTimeout)
}

func scpFile(conf *easyssh.SSHConfig, local, remote string) {
	err := conf.Scp(local, remote)
	if err != nil {
		panic(err)
	}
}

// NewServerName returns a random pet name for our server, which is a pet,
// albeit not a long-lived one. Sorry, little fella :(
func NewServerName() string {
	mathRand.Seed(time.Now().UTC().UnixNano())
	return petname.Generate(2, "-")
}

// GetProjectIDByName ...
func GetProjectIDByName(c *packngo.Client, name string) (string, error) {
	projects, _, err := c.Projects.List()
	if err != nil {
		return "", err
	}
	for _, p := range projects {
		if p.Name == name {
			return p.ID, nil
		}
	}
	return "", errors.New("project not found")

}

func GetDeviceIDByName(c *packngo.Client, projectID string, name string) (*packngo.Device, error) {

	devices, _, err := c.Devices.List(projectID)
	if err != nil {
		return nil, err
	}

	for _, dev := range devices {
		if dev.Hostname == name {
			return &dev, nil
		}
	}

	return nil, errors.New("device name not found")
}

// NewCloudInitConfig templates a cloud-init config that we pass to the packet API
// upon launching our server. We make a user and some authorized ssh keys so we
// can log in to complete provisioning (or inspect our build).
func NewCloudInitConfig(pubKey string) (string, error) {
	data, err := ioutil.ReadFile(pubKey)
	if err != nil {
		return "", err
	}
	tmpl := `#cloud-config
users:
  - name: coleman
    groups: sudo
    shell: /bin/bash
    sudo: ['ALL=(ALL) NOPASSWD:ALL']
    ssh-authorized-keys:
      - {{ .Key }}
`
	t, err := template.New("cloud-init").Parse(tmpl)
	if err != nil {
		return "", err
	}
	type Data struct {
		Key string
	}
	buf := bytes.NewBuffer([]byte(""))
	err = t.Execute(buf, Data{strings.Replace(string(data), "\n", "", -1)})
	if err != nil {
		return "", err
	}

	return buf.String(), nil
}

// MakeSSHKeyPair ...
func MakeSSHKeyPair(pubKeyPath, privateKeyPath string) error {
	privateKey, err := rsa.GenerateKey(rand.Reader, 1024)
	if err != nil {
		return err
	}

	// generate and write private key as PEM
	privateKeyFile, err := os.Create(privateKeyPath)
	defer privateKeyFile.Close()
	if err != nil {
		return err
	}
	privateKeyPEM := &pem.Block{Type: "RSA PRIVATE KEY", Bytes: x509.MarshalPKCS1PrivateKey(privateKey)}
	if err := pem.Encode(privateKeyFile, privateKeyPEM); err != nil {
		return err
	}

	// generate and write public key
	pub, err := ssh.NewPublicKey(&privateKey.PublicKey)
	if err != nil {
		return err
	}
	return ioutil.WriteFile(pubKeyPath, ssh.MarshalAuthorizedKey(pub), 0655)
}
