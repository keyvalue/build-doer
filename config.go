package main

// Config ...
type Config struct {
	Tasks        []Task `toml:"task"`
	PacketConfig PacketConfig
}

// Task ...
type Task struct {
	Repo string
	Cmd  string
}

// PacketConfig ...
type PacketConfig struct {
	Token string
}
